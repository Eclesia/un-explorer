
package unp.explorer;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.Frame;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.geometry.api.Extent;


/**
 *
 * @author Johann Sorel
 */
public class WSlideShowConfig extends WContainer {

    private final WTextField rootPath = new WTextField();
    private final WCheckBox subFolders = new WCheckBox();
    private final WSpinner timing = new WSpinner(new NumberSpinnerModel(Integer.class, 3, 1, 100000, 1), 2);
    private final WCheckBox rotate = new WCheckBox();
    private final WCheckBox images = new WCheckBox();
    private final WCheckBox videos = new WCheckBox();
    private final WButton start = new WButton(new Chars("Start"),null,new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                @Override
                public void run() {
                    try {
                        WSlideShowConfig.this.start();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }.start();
        }
    });

    public WSlideShowConfig() {

        subFolders.setCheck(true);
        images.setCheck(true);
//        videos.setCheck(true);

        final FormLayout layout = new FormLayout();
        layout.setDefaultRowSpace(4);
        layout.setDefaultColumnSpace(4);
        layout.setColumnSize(2, FormLayout.SIZE_EXPAND);
        layout.setRowSize(6, FormLayout.SIZE_EXPAND);
        setLayout(layout);
        getStyle().getSelfRule().setProperties(new Chars("margin:10;"));
        
        int y = 0;

        addChild(new WLabel(new Chars("Folder")), FillConstraint.builder().coord(0, y).build());
        addChild(rootPath, FillConstraint.builder().coord(1, y).span(3, 1).fill(true, false).build());

        y++;
        addChild(new WLabel(new Chars("Include sub-folders")), FillConstraint.builder().coord(0, y).build());
        addChild(subFolders, FillConstraint.builder().coord(1, y).build());

        y++;
        addChild(new WLabel(new Chars("Speed (seconds)")), FillConstraint.builder().coord(0, y).build());
        addChild(timing, FillConstraint.builder().coord(1, y).build());

        y++;
        addChild(new WLabel(new Chars("Rotate")), FillConstraint.builder().coord(0, y).build());
        addChild(rotate, FillConstraint.builder().coord(1, y).build());

        y++;
        addChild(new WLabel(new Chars("Images")), FillConstraint.builder().coord(0, y).build());
        addChild(images, FillConstraint.builder().coord(1, y).build());

        y++;
        addChild(new WLabel(new Chars("Videos")), FillConstraint.builder().coord(0, y).build());
        addChild(videos, FillConstraint.builder().coord(1, y).build());

        y++;
        y++;
        addChild(start, FillConstraint.builder().coord(3, y).build());

    }

    private void start() throws IOException{

        final Path path = Paths.resolve(rootPath.getText().toChars());
        final int speed = (Integer)timing.getValue() * 1000;
        final boolean includeSub = subFolders.isCheck();

        final UIFrame frame = (UIFrame) getFrame().getManager().createFrame(false);
        frame.setVisible(true);
        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:#000000;};"));
        frame.setSize(800, 600);
        frame.setState(Frame.STATE_FULLSCREEN);

//        frame.addEventListener(KeyEvent.class, new EventListener() {
//            public void receiveEvent(Event event) {
//                frame.dispose();
//            }
//        });



        final Sequence children = new ArraySequence();
        listAll(path, children, includeSub);
        System.out.println(children.getSize());

        new Thread(){

            @Override
            public void run() {
                frame.getContainer().addEventListener(new PropertyPredicate(WContainer.PROPERTY_EFFECTIVE_EXTENT), new EventListener(){
                    public void receiveEvent(Event event) {
                        fit(frame);
                    }
                });

//                while(true){
//                    final int i = (int) (Math.random() * (children.getSize()-1));
//                    final Path path = (Path) children.get(i);
//                    
//                    if(path.getName().endsWith(new Chars("gif"))){
//                        try{
//                            final WMediaPreview prev = new WMediaPreview(path);
//                            prev.setLayoutConstraint(BorderConstraint.CENTER);
//                            Thread.sleep(speed);
//                            frame.getContainer().setLayout(new AbsoluteLayout());
//                            frame.getContainer().getChildren().replaceAll(new Node[]{prev});
//                            fit(frame);
//                        }catch(Throwable ex){
//                            ex.printStackTrace();
//                        }
//                    }else{
//                        try{
//                            Image img = Images.read(path);
//
////                            if(img.getExtent().get(1)> 1200 && img.getExtent().get(0)/img.getExtent().get(1) <= 1){
//
//                            if(rotate.isCheck()){
//                                img = new Rotate270Operator().execute(img);
//                            }
//
//                            final WGraphicImage graphic = new WGraphicImage();
//                            graphic.setLayoutConstraint(BorderConstraint.CENTER);
//
//                            Extent.Long extent = img.getExtent();
//                            graphic.setFitting(bestFitting(extent, frame.getContainer().getEffectiveExtent()));
//                            graphic.setImage(img);
//                            Thread.sleep(speed);
//
//                            frame.getContainer().getChildren().replaceAll(new Node[]{graphic});
////                            }
//                        }catch(Throwable ex){
//                            ex.printStackTrace();
//                        }
//                    }
//
//                }
            }
        }.start();


        //close this frame
        //getFrame().dispose();
    }

    private void fit(Frame frame){
//        Node[] children = (Node[]) frame.getContainer().getChildren().toArray(Node.class);
//        if(children.length>0 && children[0] instanceof WMediaPreview){
//            final WMediaPreview prev = (WMediaPreview) children[0];
//
//            Extent.Long candidate = prev.getMediaExtent().copy();
//            Extent view = frame.getContainer().getEffectiveExtent().copy();
//            if(rotate.isCheck()){
//                view = new Extent.Double(view.get(1), view.get(0));
//
//                prev.getNodeTransform().getRotation().set(new double[]{
//                    0,1,
//                    -1,0});
//                prev.getNodeTransform().notifyChanged();
//            }
//
//            prev.setFitting(bestFitting(candidate,view));
//            final Extents extents = prev.getOverrideExtents();
//            extents.bestX = view.get(0);
//            extents.bestY = view.get(1);
//            prev.setOverrideExtents(extents);
//
//        }

    }

    private static int bestFitting(Extent.Long candidate, Extent view){
        final double ratioXY = candidate.get(0) / candidate.get(1);
        final double viewXY = view.get(0) / view.get(1);

        final double diff = ratioXY - viewXY;


        if(diff>-0.1 && diff<0.1){
            return WGraphicImage.FITTING_STRETCHED;
        }else if(diff > -0.8 && diff < 0.8){
            return WGraphicImage.FITTING_ZOOMED;
        }else {
            return WGraphicImage.FITTING_SCALED;
        }

    }

    private static final Chars[] VALID_IMGS = new Chars[]{
        new Chars("png"),
        new Chars("jpeg"),
        new Chars("jpg"),
        new Chars("bmp")
    };
    
    private static final Chars[] VALID_VIDEOS = new Chars[]{
        new Chars("gif")
    };

    private void listAll(Path p, Sequence list, boolean includeSub) throws IOException{
        Chars name = p.getName().toLowerCase();
        if(p.isContainer()){
            final Iterator ite = p.getChildren().createIterator();
            while(ite.hasNext()){
                final Path c = (Path) ite.next();
                if(c.isContainer()){
                    if(includeSub){
                        listAll(c, list, includeSub);
                    }
                }else{
                    listAll(c, list, includeSub);
                }
            }
        } else {
            if (images.isCheck()) {
                for(Chars end : VALID_IMGS) {
                    if(name.endsWith(end)) {
                        list.add(p);
                        break;
                    }
                }
            } else if (videos.isCheck()) {
                for(Chars end : VALID_VIDEOS) {
                    if(name.endsWith(end)) {
                        list.add(p);
                        break;
                    }
                }
            }
        }
    }

    public static void show(final Frame parent, final Path path){

        new Thread(){
            @Override
            public void run() {
                final WSlideShowConfig config = new WSlideShowConfig();
                config.rootPath.setText(new Chars(path.toURI()));
                //config.start.doClick();

                final UIFrame frame = (UIFrame) parent.getManager().createFrame(false);
                frame.addEventListener(new PropertyPredicate(Frame.PROP_VISIBLE), new EventListener() {
                    public void receiveEvent(Event event) {
                        if(((PropertyMessage)event.getMessage()).getNewValue() == Boolean.FALSE){
                            System.exit(0);
                        }
                    }
                });
                frame.getContainer().setLayout(new BorderLayout());
                frame.getContainer().addChild(config, BorderConstraint.CENTER);

                frame.setTitle(new Chars("SlideShow"));
                frame.setSize(320, 300);
                frame.setVisible(true);
            }

        }.start();
        
    }

}
