
package unp.explorer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.DefaultProperty;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.PathView;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_SELECTED_PATHS;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_VIEW_ROOT;
import science.unlicense.engine.ui.component.path.WInteractiveView;
import science.unlicense.engine.ui.component.path.WListView;
import science.unlicense.engine.ui.component.path.WPreviewView;
import science.unlicense.engine.ui.component.path.WTableView;
import science.unlicense.engine.ui.component.path.WTreePathView;
import science.unlicense.engine.ui.model.CheckGroup;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuSwitch;


/**
 *
 * @author Johann Sorel
 */
public class WExplorer extends WContainer {

    private static final Chars FONT_STYLE = new Chars("{subs:{\nborder:none\nfont:{\nfamily:font('Unicon' 14 'none')\n}\n}\n}");

    //top bar
    private final WMenuButton goToHome = new WMenuButton(new Chars("\uE024"), null,new EventListener() {
        public void receiveEvent(Event event) {
            Object value = science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home")).getValue();
            setViewRoot(Paths.resolve((Chars)value));
        }
    });
    private final WTextField pathText = new WTextField();
    private final WMenuButton slideShow = new WMenuButton(new Chars("\uE02B"), null,new EventListener() {
        public void receiveEvent(Event event) {
            WSlideShowConfig.show(WExplorer.this.getFrame(),getViewRoot());
        }
    });
    private final WContainer leftViewsButtons = new WContainer(new GridLayout(1, -1));
    private final WContainer centerViewsButtons = new WContainer(new GridLayout(1, -1));
        
    //predicate informations
    private final Sequence predicates = new ArraySequence();
    private final RowModel predModel = new DefaultRowModel(predicates);
    private final WSelect predSelect = new WSelect(predModel);

    //views
    private final WSplitContainer centerSplit = new WSplitContainer();
    private PathView[] leftViews = new PathView[0];
    private WSwitch[] leftSwitchs = new WSwitch[0];
    private PathView[] centerViews = new PathView[0];
    private WSwitch[] centerSwitchs = new WSwitch[0];
    //currently active views
    private PathView leftView = null;
    private PathView centerView = null;

    public WExplorer() {
        super(new BorderLayout());
        predicates.add(Predicate.TRUE);
        
        goToHome.getStyle().addRules(FONT_STYLE);
        slideShow.getStyle().addRules(FONT_STYLE);
        predSelect.setVisible(false);
        
        //top panel
        final WButtonBar topPane = new WButtonBar();
        ((FormLayout)topPane.getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);
        topPane.addChild(leftViewsButtons, FillConstraint.builder().coord(0, 0).build());
        topPane.addChild(goToHome, FillConstraint.builder().coord(1, 0).build());
        topPane.addChild(pathText, FillConstraint.builder().coord(2, 0).build());
        topPane.addChild(centerViewsButtons, FillConstraint.builder().coord(3, 0).build());
        topPane.addChild(slideShow, FillConstraint.builder().coord(4, 0).build());
                
        //bottom panel
        //predicates list
        final WContainer predPane = new WContainer(new BorderLayout());
        predPane.addChild(predSelect,BorderConstraint.CENTER);      

        //add all blocks
        addChild(topPane,BorderConstraint.TOP);
        addChild(centerSplit,BorderConstraint.CENTER);
        
        setLeftViews(new PathView[]{new WTreePathView()});
        setCenterViews(new PathView[]{
            new WListView(),
            new WTableView(),
            new WPreviewView(),
            new WInteractiveView()});
    }

    /**
     * Set the possible left side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setLeftViews(PathView[] views){
        leftViewsButtons.getChildren().removeAll();
        setSelectedLeftView(null);
        
        if(views==null) views = new PathView[0];
        this.leftViews = views.clone();
                
        if(leftViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            leftSwitchs = new WSwitch[views.length];
            for(int i=0;i<views.length;i++){
                final PathView pv = views[i];
                leftSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if(((WSwitch)event.getSource()).isCheck()){
                            setSelectedLeftView(pv);
                        }
                    }
                });
                group.add(leftSwitchs[i]);
                if(views.length>1) leftViewsButtons.getChildren().add(leftSwitchs[i]);
            }
            
            setSelectedLeftView(leftViews[0]);
        }
    }
    
    /**
     * Get currently used left side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getLeftViews(){
        return leftViews.clone();
    }
    
    /**
     * Set active left side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedLeftView(PathView view){
        if(leftView==view) return;
        if(view!=null && !Arrays.contains(leftViews, view)){
            throw new IllegalArgumentException("View is not in the list");
        }
        //remove previous view
        if(leftView!=null){
            leftView.varViewRoot().unsync();
            leftView.varSelectedPath().unsync();
            centerSplit.getChildren().remove(leftView.getViewWidget());
        }
        this.leftView = view;
        leftSwitchs[Arrays.getFirstOccurence(leftViews, 0, leftViews.length, view)].setCheck(true);
        //add new view
        if(leftView!=null){
            leftView.varViewRoot().sync(varViewRoot());
            leftView.varSelectedPath().sync(varSelectedPath());
            centerSplit.addChild(leftView.getViewWidget(), SplitConstraint.LEFT);
        }
    }
    
    /**
     * Get active left side view.
     * @return PathView, can be null
     */
    public PathView getSelectedLeftView(){
        return leftView;
    }
    
    /**
     * Set the possible center side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setCenterViews(PathView[] views){
        centerViewsButtons.getChildren().removeAll();
        setSelectedCenterView(null);
        
        if(views==null) views = new PathView[0];
        this.centerViews = views.clone();
                
        if(centerViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            centerSwitchs = new WSwitch[views.length];
            for(int i=0;i<views.length;i++){
                final PathView pv = views[i];
                centerSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if(((WSwitch)event.getSource()).isCheck()){
                            setSelectedCenterView(pv);
                        }
                    }
                });
                group.add(centerSwitchs[i]);
                if(views.length>1) centerViewsButtons.getChildren().add(centerSwitchs[i]);
            }
            
            setSelectedCenterView(centerViews[0]);
        }
    }
    
    /**
     * Get currently used center side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getCenterViews(){
        return centerViews.clone();
    }
    
    /**
     * Set active center side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedCenterView(PathView view){
        if(centerView==view) return;
        if(view!=null && !Arrays.contains(centerViews, view)){
            throw new IllegalArgumentException("View is not in the list");
        }
        //remove previous view
        if(centerView!=null){
            centerView.varViewRoot().unsync();
            centerView.varSelectedPath().unsync();
            centerSplit.getChildren().remove(centerView.getViewWidget());
        }
        this.centerView = view;
        centerSwitchs[Arrays.getFirstOccurence(centerViews, 0, centerViews.length, view)].setCheck(true);
        //add new view
        if(centerView!=null){
            centerView.varViewRoot().sync(varViewRoot());
            centerView.varSelectedPath().sync(varSelectedPath());
            centerSplit.addChild(centerView.getViewWidget(), SplitConstraint.RIGHT);
        }
    }
    
    /**
     * Get active center side view.
     * @return PathView, can be null
     */
    public PathView getSelectedCenterView(){
        return centerView;
    }

    /**
     * {@inheritDoc }
     */
    public void setViewRoot(Path viewRoot) {
        if(setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot)){
            if(viewRoot==null){
                pathText.setText(Chars.EMPTY);
            }else{
                pathText.setText(new Chars(viewRoot.toURI()));
            }
        }
    }

    /**
     * {@inheritDoc }
     */
    public Path getViewRoot() {
        return (Path) getPropertyValue(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public Property varViewRoot(){
        return new DefaultProperty(this, PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public void setSelectedPath(Path[] path) {
        setPropertyValue(PROPERTY_SELECTED_PATHS, path);
    }

    /**
     * {@inheritDoc }
     */
    public Path[] getSelectedPath() {
        return (Path[]) getPropertyValue(PROPERTY_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public Property varSelectedPath(){
        return new DefaultProperty(this, PROPERTY_SELECTED_PATHS);
    }

    public void setSelectedPredicate(Predicate predicate) {
        predSelect.setSelection(predicate);
    }

    public Predicate getSelectedPredicate() {
        return (Predicate) predSelect.getSelection();
    }

    public void setPredicates(Sequence predicates){
        final Predicate selected = getSelectedPredicate();
        this.predicates.replaceAll(predicates);
        setSelectedPredicate(selected);
        if(predicates.isEmpty() || (predicates.getSize()==1 && predicates.get(0) == Predicate.TRUE)){
            predSelect.setVisible(false);
        }else{
            predSelect.setVisible(true);
        }
    }
    
}
